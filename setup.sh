#!/bin/bash
HEIGHT=15
WIDTH=50
CHOICE_HEIGHT=10
BACKTITLE="Returned From Menu"
TITLE="SMG Player Setup"
MENU="Choose one of the following options:"
MENUSELECTION=1 
SLEEP_PERIOD=2
CONFIGFILE="config.ini"
OPTIONS=(
		"s" "General Settings"
		"d" "DJ Settings"
		"p" "Proxy Settings"
		"i" "IceCast Settings"
		"m" "Multi Output Settings"
        "u" "Subscribe Events"
        q "Exit Q")


function checkOS(){
	if [ -f /etc/os-release ]; then
		# freedesktop.org and systemd
		. /etc/os-release
		OS=$NAME
		VER=$VERSION_ID
	elif type lsb_release >/dev/null 2>&1; then
		# linuxbase.org
		OS=$(lsb_release -si)
		VER=$(lsb_release -sr)
	elif [ -f /etc/lsb-release ]; then
		# For some versions of Debian/Ubuntu without lsb_release command
		. /etc/lsb-release
		OS=$DISTRIB_ID
		VER=$DISTRIB_RELEASE
	elif [ -f /etc/debian_version ]; then
		# Older Debian/Ubuntu/etc.
		OS=Debian
		VER=$(cat /etc/debian_version)
	elif [ -f /etc/SuSe-release ]; then
		# Older SuSE/etc.
		...
	elif [ -f /etc/redhat-release ]; then
		# Older Red Hat, CentOS, etc.
		...
	else
		# Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
		OS=$(uname -s)
		VER=$(uname -r)
	fi
	case $(uname -m) in
	x86_64)
		BITS=64
		;;
	i*86)
		BITS=32
		;;
	*)
		BITS=?
		;;
	esac
	ARCH=$(uname -m)
	echo "OS=$OS, VER=$VER"
}
function findAndReplace(){
 
	local INISECTION=$(echo "$1"|sed 's#/#\\/#g'); shift
    local INIPARAMETER=$(echo "$1"|sed 's#/#\\/#g'); shift
    local TEXTREPLACE=$(echo "$1"|sed 's#/#\\/#g'); shift
	#echo "replacing $INISECTION/$INIPARAMETER with $TEXTREPLACE"
	sed -i "/^\[$INISECTION\]$/,/^\[/ s/^$INIPARAMETER/$TEXTREPLACE/" ./$CONFIGFILE
}

function getValue(){
	local INISECTION=$1; shift
    local INIPARAMETER=$1; shift
	echo $(sed -nr "/^\[${INISECTION}\]/ { :l /^${INIPARAMETER}[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" ./$CONFIGFILE)
}

function findCheckReplace(){
	local INISECTION=$1; shift
    local INIPARAMETER=$1; shift
    local TEXTFIND=$1; shift
    local TEXTREPLACE=$1; 

	local res=$(getValue $INISECTION $INIPARAMETER)
	#echo "returned $res , $TEXTFIND to find, $TEXTREPLACE to replace"
	if [ "$res" == $TEXTFIND ]; then 
		echo "$INISECTION/$INIPARAMETER was '$TEXTFIND' replacing with '$TEXTREPLACE'"
		 findAndReplace "$INISECTION" "$INIPARAMETER=$TEXTFIND" "$INIPARAMETER=$TEXTREPLACE"
	else
		echo "$INISECTION/$INIPARAMETER is already '$TEXTFIND'"
	fi
}

function showInputbox(){
  	local INISECTION=$1; shift
    local INIPARAMETER=$1; 
	local res=$(getValue $INISECTION $INIPARAMETER)
	echo "$INISECTION/$INIPARAMETER value is '$res'"
	DIALOG=${DIALOG=dialog}
	tempfile=`tempfile 2>/dev/null` || tempfile=/tmp/test$$
	trap "rm -f $tempfile" 0 1 2 5 15

	
	
	CHOICE=$(dialog --title "$INISECTION/$INIPARAMETER" --clear \
					--inputbox "Please enter new $INIPARAMETER\n Current value:'$res'" \
					16 51 2>&1 >/dev/tty)
	MENUSELECTION=$? #1 cancel, 0-ok, 255-esc
	if [ "$MENUSELECTION" == "0" ]; then 
		if [ "$res" == $CHOICE ]; then 
			echo "$INISECTION/$INIPARAMETER was '$res' new value is '$CHOICE'.. nothing changed.."
		else
			echo "$INISECTION/$INIPARAMETER was '$res' replacing with '$CHOICE'"
			 findAndReplace "$INISECTION" "$INIPARAMETER=$res" "$INIPARAMETER=$CHOICE"
		fi
	else
		echo "no selection made"
	fi
}
 

function settingsMenu(){
	LMENU_TITLE="General Settings"
	INISECTION="Settings"
	local deviceID=$(getValue "$INISECTION" "deviceID")
	local soundCard=$(getValue "$INISECTION" "soundCard")
	local serviceUrl=$(getValue "$INISECTION" "serviceUrl")
	local downloadURL=$(getValue "$INISECTION" "downloadURL")
	local NextButtonVisible=$(getValue "$INISECTION" "NextButtonVisible")
	local ServicePort=$(getValue "$INISECTION" "ServicePort")
	local DisableWebserver=$(getValue "$INISECTION" "DisableWebserver")


	dialog --backtitle "Exit $LMENU_TITLE" --title "SMG Player $LMENU_TITLE" \
	--form "\nEnter Settings and Select OK" 20 70 7 \
	"deviceID:" 1 1 "$deviceID" 1 20 10 6  \
	"soundCard:" 2 1 "$soundCard" 2 20 5 5  \
	"serviceUrl:" 3 1 "$serviceUrl" 3 20 40 60  \
	"downloadURL:" 4 1 "$downloadURL" 4 20 40 60  \
	"NextButtonVisible:" 5 1 "$NextButtonVisible" 5 20 2 2  \
	"DisableWebserver:" 6 1 "$DisableWebserver" 6 20 2 2  \
	"ServicePort:" 7 1 "$ServicePort" 7 20 10 10  \
	> /tmp/out.tmp \
	2>&1 >/dev/tty
	MENUSELECTION=$? #1 cancel, 0-ok, 255-esc
	# Start retrieving each line from temp file 1 by one with sed and declare variables as inputs
	input1=`sed -n 1p /tmp/out.tmp`
	input2=`sed -n 2p /tmp/out.tmp`
	input3=`sed -n 3p /tmp/out.tmp`
	input4=`sed -n 4p /tmp/out.tmp`
	input5=`sed -n 5p /tmp/out.tmp`
	input6=`sed -n 6p /tmp/out.tmp`
	input7=`sed -n 7p /tmp/out.tmp`
	# remove temporary file created
	rm -f /tmp/out.tmp
	#Write to output file the result
	echo $input1 , $input2 , $input3 , $input4 , $input5 , $input6 , $input7
	
	if [ "$MENUSELECTION" == "0" ]; then 
			 findAndReplace "$INISECTION" "deviceID=$deviceID" "deviceID=$input1"
			 findAndReplace "$INISECTION" "soundCard=$soundCard" "soundCard=$input2"
			 findAndReplace "$INISECTION" "serviceUrl=$serviceUrl" "serviceUrl=$input3"
			 findAndReplace "$INISECTION" "downloadURL=$downloadURL" "downloadURL=$input4"
			 findAndReplace "$INISECTION" "NextButtonVisible=$NextButtonVisible" "NextButtonVisible=$input5"
			 findAndReplace "$INISECTION" "DisableWebserver=$DisableWebserver" "DisableWebserver=$input6"
			 findAndReplace "$INISECTION" "ServicePort=$ServicePort" "ServicePort=$input7"
	else
		echo "no selection made"
	fi
}


function iceMenu(){
	LMENU_TITLE="IceCast Settings"
	INISECTION="iceCast"
	local isEnabled=$(getValue "$INISECTION" "isEnabled")
	local ServerAddress=$(getValue "$INISECTION" "ServerAddress")
	local ServerPort=$(getValue "$INISECTION" "ServerPort")
	local ServerPass=$(getValue "$INISECTION" "ServerPass")
	local ServerMountPoint=$(getValue "$INISECTION" "ServerMountPoint")
	local Bitrate=$(getValue "$INISECTION" "Bitrate")
	local Mode=$(getValue "$INISECTION" "Mode")
	local Quality=$(getValue "$INISECTION" "Quality")
	local SampleRate=$(getValue "$INISECTION" "SampleRate")
	local hasLocalOutput=$(getValue "$INISECTION" "hasLocalOutput")


	dialog --backtitle "Exit $LMENU_TITLE" --title "SMG Player $LMENU_TITLE" \
	--form "\nEnter Settings and Select OK" 20 70 7 \
	"isEnabled:" 1 1 "$isEnabled" 1 20 2 2  \
	"ServerAddress:" 2 1 "$ServerAddress" 2 20 30 30  \
	"ServerPort:" 3 1 "$ServerPort" 3 20 6 6  \
	"ServerPass:" 4 1 "$ServerPass" 4 20 10 10  \
	"ServerMountPoint:" 5 1 "$ServerMountPoint" 5 20 10 10  \
	"Bitrate:" 6 1 "$Bitrate" 6 20 5 5  \
	"Channel Count:" 7 1 "$Mode" 7 20 2 2  \
	"Quality:" 8 1 "$Quality" 8 20 6 6  \
	"SampleRate:" 9 1 "$SampleRate" 9 20 6 6  \
	"hasLocalOutput:" 10 1 "$hasLocalOutput" 10 20 2 2  \
	> /tmp/out.tmp \
	2>&1 >/dev/tty
	MENUSELECTION=$? #1 cancel, 0-ok, 255-esc
	# Start retrieving each line from temp file 1 by one with sed and declare variables as inputs
	input1=`sed -n 1p /tmp/out.tmp`
	input2=`sed -n 2p /tmp/out.tmp`
	input3=`sed -n 3p /tmp/out.tmp`
	input4=`sed -n 4p /tmp/out.tmp`
	input5=`sed -n 5p /tmp/out.tmp`
	input6=`sed -n 6p /tmp/out.tmp`
	input7=`sed -n 7p /tmp/out.tmp`
	input8=`sed -n 8p /tmp/out.tmp`
	input9=`sed -n 9p /tmp/out.tmp`
	input10=`sed -n 10p /tmp/out.tmp`
	# remove temporary file created
	rm -f /tmp/out.tmp
	#Write to output file the result
	 
	echo "menu selection is $MENUSELECTION"
	if [ "$MENUSELECTION" == "0" ]; then 
			 findAndReplace "$INISECTION" "isEnabled=$isEnabled" "isEnabled=$input1"
			 findAndReplace "$INISECTION" "ServerAddress=$ServerAddress" "ServerAddress=$input2"
			 findAndReplace "$INISECTION" "ServerPort=$ServerPort" "ServerPort=$input3"
			 findAndReplace "$INISECTION" "ServerPass=$ServerPass" "ServerPass=$input4"
			 findAndReplace "$INISECTION" "ServerMountPoint=$ServerMountPoint" "ServerMountPoint=$input5"
			 findAndReplace "$INISECTION" "Bitrate=$Bitrate" "Bitrate=$input6"
			 findAndReplace "$INISECTION" "Mode=$Mode" "Mode=$input7"
			 findAndReplace "$INISECTION" "Quality=$Quality" "Quality=$input8"
			 findAndReplace "$INISECTION" "SampleRate=$SampleRate" "SampleRate=$input9"
			 findAndReplace "$INISECTION" "hasLocalOutput=$hasLocalOutput" "hasLocalOutput=$input10"
	else
		echo "no selection made"
	fi
}


function proxyMenu(){
	LMENU_TITLE="Proxy Settings"
	INISECTION="proxy"
	local proxyEnabled=$(getValue "$INISECTION" "proxyEnabled")
	local proxyHost=$(getValue "$INISECTION" "proxyHost")
	local proxyUser=$(getValue "$INISECTION" "proxyUser")
	local proxyPass=$(getValue "$INISECTION" "proxyPass")
	local proxyPort=$(getValue "$INISECTION" "proxyPort")



	dialog --backtitle "Exit $LMENU_TITLE" --title "SMG Player $LMENU_TITLE" \
	--form "\nEnter Settings and Select OK" 20 70 7 \
	"proxyEnabled:" 1 1 "$proxyEnabled" 1 20 2 2  \
	"proxyHost:" 2 1 "$proxyHost" 2 20 30 30  \
	"proxyUser:" 3 1 "$proxyUser" 3 20 6 6  \
	"proxyPass:" 4 1 "$proxyPass" 4 20 10 10  \
	"proxyPort:" 5 1 "$proxyPort" 5 20 10 10  \
	> /tmp/out.tmp \
	2>&1 >/dev/tty
	MENUSELECTION=$? #1 cancel, 0-ok, 255-esc
	# Start retrieving each line from temp file 1 by one with sed and declare variables as inputs
	input1=`sed -n 1p /tmp/out.tmp`
	input2=`sed -n 2p /tmp/out.tmp`
	input3=`sed -n 3p /tmp/out.tmp`
	input4=`sed -n 4p /tmp/out.tmp`
	input5=`sed -n 5p /tmp/out.tmp`

	rm -f /tmp/out.tmp
	#Write to output file the result
	 
	echo "menu selection is $MENUSELECTION"
	if [ "$MENUSELECTION" == "0" ]; then 
			 findAndReplace "$INISECTION" "proxyEnabled=$proxyEnabled" "proxyEnabled=$input1"
			 findAndReplace "$INISECTION" "proxyHost=$proxyHost" "proxyHost=$input2"
			 findAndReplace "$INISECTION" "proxyUser=$proxyUser" "proxyUser=$input3"
			 findAndReplace "$INISECTION" "proxyPass=$proxyPass" "proxyPass=$input4"
			 findAndReplace "$INISECTION" "proxyPort=$proxyPort" "proxyPort=$input5"

	else
		echo "no selection made"
	fi
}


function menu(){

	CHOICE=$(dialog --clear \
					--backtitle "$BACKTITLE" \
					--title "$TITLE" \
					--menu "($OS $VER ${BITS}bit $ARCH)\n$MENU" \
					$HEIGHT $WIDTH $CHOICE_HEIGHT \
					"${OPTIONS[@]}" \
					2>&1 >/dev/tty)

	clear
	case $CHOICE in
			i)
				iceMenu
				menu
				;;
			s)
				settingsMenu
				menu
				;;
			p)
				proxyMenu
				menu
				;;
			2)
				echo "You chose Option 2"
				
				;;
			3)
				echo "Bye"; break;;
	esac
}

checkOS
menu